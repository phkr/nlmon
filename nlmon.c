/*
 * Copy me if you can.
 * by 20h
 */

#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <poll.h>
#include <ctype.h>
#include <string.h>
#include <errno.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <linux/types.h>
#include <linux/netlink.h>

#include "arg.h"

char *argv0;

void
edie(char *fmt, ...)
{
	va_list fmtargs;

	va_start(fmtargs, fmt);
	vfprintf(stderr, fmt, fmtargs);
	va_end(fmtargs);
	fprintf(stderr, ": ");

	perror(NULL);

	exit(1);
}

void
die(char *fmt, ...)
{
	va_list fmtargs;

	va_start(fmtargs, fmt);
	vfprintf(stderr, fmt, fmtargs);
	va_end(fmtargs);

	exit(1);
}

void
usage(void)
{
	die("usage: %s [-h] [-kl] [-f subsystem]\n", argv0);
}

int
main(int argc, char *argv[])
{
	struct sockaddr_nl nls, cnls;
	struct pollfd fds;
	struct msghdr hdr;
	struct iovec iov;
	char buf[4097], obuf[4098], *subsystem;
	int i, len, olen, slen, showudev, showkernel;

	showkernel = 1;
	showudev = 1;
	subsystem = NULL;

	ARGBEGIN {
	case 'f':
		subsystem = EARGF(usage());
		break;
	case 'k':
		showudev = 0;
		break;
	case 'l':
		showkernel = 0;
		break;
	default:
		usage();
	} ARGEND;

	memset(&nls, 0, sizeof(nls));
	nls.nl_family = AF_NETLINK;
	nls.nl_pid = getpid();
	nls.nl_groups = -1;

	/*
	 * The whole process of decoding the kernel messages is
	 * kept simple to avoid clashes with root rights. One
	 * goal of nlmon is to be able to decode the kernel
	 * messages in userspace.
	 */

	fds.events = POLLIN;
	fds.fd = socket(PF_NETLINK, SOCK_DGRAM, NETLINK_KOBJECT_UEVENT);
	if (fds.fd < 0)
		edie("socket");

	/*
	 * Setting the sockopts here will require root rights.
	 * Just keep it that way and pretend to have a big
	 * enough buffer.
	 */

	if (bind(fds.fd, (void *)&nls, sizeof(nls)))
		edie("bind");

	buf[sizeof(buf)-1] = '\0';
	while (poll(&fds, 1, -1) > -1) {
		iov.iov_base = &buf;
		iov.iov_len = sizeof(buf);
		memset(&hdr, 0, sizeof(hdr));
		hdr.msg_iov = &iov;
		hdr.msg_iovlen = 1;
		hdr.msg_name = &cnls;
		hdr.msg_namelen = sizeof(cnls);

		len = recvmsg(fds.fd, &hdr, 0);
		if (len < 0) {
			if (errno == EINTR)
				continue;
			edie("recvmsg");
		}
		if (len < 32 || len >= sizeof(buf))
			continue;

		if (!memcmp(buf, "libudev", 8)) {
			if (!showudev)
				continue;
		} else {
			if (!showkernel)
				continue;

			/*
			 * Kernel messages should be from root.
			 */
			if (cnls.nl_pid > 0)
				continue;
		}

		for (i = 0, olen = 0; i < len; i += slen + 1) {
			slen = strlen(buf+i);
			if (!slen || !strchr(buf+i, '='))
				continue;
			if (subsystem && !strncmp(buf+i, "SUBSYSTEM=", 10)
					&& !strstr(buf+i+10, subsystem)) {
				olen = 0;
				break;
			}

			snprintf(obuf+olen, sizeof(obuf)-olen-2,
					"%s\n", buf+i);
			olen += slen + 1;
		}
		if (olen > 0) {
			obuf[olen] = '\n';
			write(1, obuf, olen+1);
		}
	}

	return 0;
}

