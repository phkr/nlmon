# nlmon metadata
NAME = nlmon
VERSION = 0.3

# Customize below to fit your system

# paths
PREFIX ?= /usr
MANPREFIX = ${PREFIX}/share/man

# includes and libs
INCS = -I. -I/usr/include
LIBS = -L/usr/lib -lc

# flags
CPPFLAGS = -DVERSION=\"${VERSION}\"
CFLAGS = -g -std=c99 -pedantic -Wall -O0 ${INCS} ${CPPFLAGS}
LDFLAGS = -g -static ${LIBS}
#LDFLAGS = -s ${LIBS}

# compiler and linker
CC = cc

